ARG BEATS_VERSION=7.10.2

FROM docker.elastic.co/beats/filebeat:$BEATS_VERSION

COPY group /etc/group

